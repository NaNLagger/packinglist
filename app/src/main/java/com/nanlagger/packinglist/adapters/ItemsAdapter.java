package com.nanlagger.packinglist.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nanlagger.packinglist.R;
import com.nanlagger.packinglist.fragments.ItemsFragment;
import com.nanlagger.packinglist.models.ItemModel;
import com.nanlagger.packinglist.models.ListModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 25.08.14.
 */
public class ItemsAdapter extends BaseAdapter {

    private final LayoutInflater lInflater;
    protected ItemModel data;
    protected ItemsFragment parentFragment;
    protected Context context;

    public ItemsAdapter(Context ctx, ItemModel data, ItemsFragment fragment) {
        this.data = data;
        this.parentFragment = fragment;
        this.context = ctx;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.items_adapter, parent, false);
        }
        final HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

        CheckBox selectedBox = (CheckBox) view.findViewById(R.id.checkBox);
        final FrameLayout checkFrame = (FrameLayout) view.findViewById(R.id.checkFrame);

        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.imageButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("Delete '" + item.get("name").toString() + "' ?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ItemModel model = new ItemModel();
                        model.deleteDB(context,"_id = " + item.get("id").toString());
                        parentFragment.updateList();
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialog.show();
            }
        });
        selectedBox.setText(item.get("name").toString());
        selectedBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked != (Boolean) item.get("selected")) {
                    item.put("selected", isChecked);
                    ItemModel model = new ItemModel();
                    model.id = (Integer) item.get("id");
                    model.selected = isChecked;
                    model.updateDB(context, null);
                    model = null;
                    if(isChecked)
                        checkFrame.setBackgroundResource(R.color.selected_true);
                    else
                        checkFrame.setBackgroundResource(R.color.selected_false);
                    ListModel updateModel = new ListModel();
                    updateModel.id = (Integer) item.get("list_id");
                    updateModel.progress = restoreProgress(updateModel.id);
                    updateModel.updateDB(context, null);
                    updateModel = null;
                    parentFragment.updateActionBar();
                }
            }
        });
        selectedBox.setChecked((Boolean) item.get("selected"));
        if(selectedBox.isChecked())
            checkFrame.setBackgroundResource(R.color.selected_true);
        else
            checkFrame.setBackgroundResource(R.color.selected_false);

        //nameTextView.setText(item.get("name").toString());
        //progressTextView.setText(item.get("progress").toString() + "%");
        return view;
    }

    public int restoreProgress(int id) {
        ItemModel model = new ItemModel();
        model.fromDB(context, "selected = 1 AND list_id = " + id);
        int selectedCount = model.data.size();
        return (int) (((float)selectedCount/(float)getCount())*100);
    }
}
