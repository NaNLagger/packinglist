package com.nanlagger.packinglist.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nanlagger.packinglist.R;
import com.nanlagger.packinglist.fragments.ListsFragment;
import com.nanlagger.packinglist.models.ItemModel;
import com.nanlagger.packinglist.models.ListModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by NaNLagger on 23.08.14.
 */

public class ListsAdapter extends BaseAdapter {

    private final LayoutInflater lInflater;
    protected ListModel data;
    protected ListsFragment parentFragment;
    protected Context context;

    public ListsAdapter(Context ctx, ListModel data, ListsFragment fragment) {
        this.data = data;
        this.parentFragment = fragment;
        this.context = ctx;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.lists_adapter, parent, false);
        }
        final HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

        TextView nameTextView = (TextView) view.findViewById(R.id.textView);
        TextView progressTextView = (TextView) view.findViewById(R.id.textView2);
        TextView dateTextView = (TextView) view.findViewById(R.id.textView3);
        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.imageButton);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("Delete '" + item.get("name").toString() + "' ?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ListModel model = new ListModel();
                        model.deleteDB(context, "_id = " + item.get("id").toString());
                        ItemModel model1 = new ItemModel();
                        model1.deleteDB(context, "list_id = " + item.get("id").toString());
                        model = null;
                        model1 = null;
                        parentFragment.updateList();
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialog.show();
            }
        });
        Date date = new Date(((Long) item.get("date"))*1000);
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String reportDate = df.format(date);
        nameTextView.setText(item.get("name").toString());
        dateTextView.setText(reportDate);
        progressTextView.setText(item.get("progress").toString() + "%");
        if ((Integer) item.get("progress") < 10)
            progressTextView.setBackgroundResource(R.color.progress_0);
        if ((Integer) item.get("progress") < 20 && (Integer) item.get("progress") >= 10)
            progressTextView.setBackgroundResource(R.color.progress_10);
        if ((Integer) item.get("progress") < 30 && (Integer) item.get("progress") >= 20)
            progressTextView.setBackgroundResource(R.color.progress_20);
        if ((Integer) item.get("progress") < 40 && (Integer) item.get("progress") >= 30)
            progressTextView.setBackgroundResource(R.color.progress_30);
        if ((Integer) item.get("progress") < 50 && (Integer) item.get("progress") >= 40)
            progressTextView.setBackgroundResource(R.color.progress_40);
        if ((Integer) item.get("progress") < 60 && (Integer) item.get("progress") >= 50)
            progressTextView.setBackgroundResource(R.color.progress_50);
        if ((Integer) item.get("progress") < 70 && (Integer) item.get("progress") >= 60)
            progressTextView.setBackgroundResource(R.color.progress_60);
        if ((Integer) item.get("progress") < 80 && (Integer) item.get("progress") >= 70)
            progressTextView.setBackgroundResource(R.color.progress_70);
        if ((Integer) item.get("progress") < 90 && (Integer) item.get("progress") >= 80)
            progressTextView.setBackgroundResource(R.color.progress_80);
        if ((Integer) item.get("progress") < 100 && (Integer) item.get("progress") >= 90)
            progressTextView.setBackgroundResource(R.color.progress_90);
        if ((Integer) item.get("progress") == 100)
            progressTextView.setBackgroundResource(R.color.progress_100);
        return view;
    }

}
