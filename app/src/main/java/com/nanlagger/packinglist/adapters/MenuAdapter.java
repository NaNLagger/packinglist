package com.nanlagger.packinglist.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nanlagger.packinglist.R;
import com.nanlagger.packinglist.fragments.ListsFragment;
import com.nanlagger.packinglist.fragments.MenuFragment;
import com.nanlagger.packinglist.models.ItemModel;
import com.nanlagger.packinglist.models.ListModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by root on 25.08.14.
 */
public class MenuAdapter extends BaseAdapter {
    private final LayoutInflater lInflater;
    protected ListModel data;
    protected Context context;

    public MenuAdapter(Context ctx, ListModel data) {
        this.data = data;
        this.context = ctx;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.menu_adapter, parent, false);
        }
        final HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

        TextView nameTextView = (TextView) view.findViewById(R.id.textView);
        TextView progressTextView = (TextView) view.findViewById(R.id.textView2);

        nameTextView.setText(item.get("name").toString());
        progressTextView.setText(item.get("progress").toString() + "%");
        if ((Integer) item.get("progress") < 10)
            progressTextView.setBackgroundResource(R.color.progress_0);
        if ((Integer) item.get("progress") < 20 && (Integer) item.get("progress") >= 10)
            progressTextView.setBackgroundResource(R.color.progress_10);
        if ((Integer) item.get("progress") < 30 && (Integer) item.get("progress") >= 20)
            progressTextView.setBackgroundResource(R.color.progress_20);
        if ((Integer) item.get("progress") < 40 && (Integer) item.get("progress") >= 30)
            progressTextView.setBackgroundResource(R.color.progress_30);
        if ((Integer) item.get("progress") < 50 && (Integer) item.get("progress") >= 40)
            progressTextView.setBackgroundResource(R.color.progress_40);
        if ((Integer) item.get("progress") < 60 && (Integer) item.get("progress") >= 50)
            progressTextView.setBackgroundResource(R.color.progress_50);
        if ((Integer) item.get("progress") < 70 && (Integer) item.get("progress") >= 60)
            progressTextView.setBackgroundResource(R.color.progress_60);
        if ((Integer) item.get("progress") < 80 && (Integer) item.get("progress") >= 70)
            progressTextView.setBackgroundResource(R.color.progress_70);
        if ((Integer) item.get("progress") < 90 && (Integer) item.get("progress") >= 80)
            progressTextView.setBackgroundResource(R.color.progress_80);
        if ((Integer) item.get("progress") < 100 && (Integer) item.get("progress") >= 90)
            progressTextView.setBackgroundResource(R.color.progress_90);
        if ((Integer) item.get("progress") == 100)
            progressTextView.setBackgroundResource(R.color.progress_100);
        return view;
    }
}
