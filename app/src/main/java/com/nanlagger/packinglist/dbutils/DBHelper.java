package com.nanlagger.packinglist.dbutils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by root on 23.08.14.
 */
public class DBHelper extends SQLiteOpenHelper{

    private static String DB_PATH = "/data/data/com.nanlagger.packinglist/databases/";

    static String DB_NAME = "packinglist.jet";

    private static final int DB_VERSION = 1;

    private static DBHelper instance = null;

    private final Context myContext;


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.myContext = context;
    }

    public static synchronized DBHelper getHelper(Context context) {
        if (instance == null)
            instance = new DBHelper(context);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE lists ("
                + "_id      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + "name     TEXT    NOT NULL, "
                + "progress INTEGER DEFAULT ( 0 ), "
                + "date     BIGINT, "
                + "runtime  BIGINT"
                + ");"
        );

        db.execSQL("CREATE TABLE items ("
                + "_id      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + "name     TEXT    NOT NULL, "
                + "list_id  INTEGER DEFAULT ( 0 ), "
                + "selected INTEGER DEFAULT ( 0 ) "
                + ");"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
