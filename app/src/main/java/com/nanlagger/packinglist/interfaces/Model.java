package com.nanlagger.packinglist.interfaces;

import android.content.Context;

import java.util.HashMap;

/**
 * Created by root on 23.08.14.
 */
public interface Model {

    HashMap<?, ?> toHashMap();

    Model fromDB(Context c, String param);

    String getSpecifiedTableName();

    Class<Model> getSpecifiedClass();
}
