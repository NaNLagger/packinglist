package com.nanlagger.packinglist.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nanlagger.packinglist.dbutils.DBHelper;
import com.nanlagger.packinglist.interfaces.Model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by NaNLagger on 23.08.14.
 */

public class ListModel implements Model {

    public Integer id; //id list
    public String name; //name list
    public Integer progress; //percent progress selected
    public Long date;
    public Long runtime;

    public ArrayList<HashMap<String, Object>> data; //Data model

    protected static final String tablename = "lists"; //tablename from DataBase

    public ListModel() {
        data = new ArrayList<HashMap<String, Object>>();
    }

    @Override
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        args.put("name", name);
        args.put("progress", progress);
        args.put("date", date);
        args.put("runtime", runtime);
        return args;
    }

    @Override
    public Model fromDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getReadableDatabase();
        String query = "SELECT * FROM " + tablename;
        if(param != null) {
            query += " WHERE " + param;
        }
        Cursor cursor = sql.rawQuery(query, null);
        if(data != null)
            data.clear();
        while (cursor.moveToNext()) {
            id = cursor.getInt(cursor.getColumnIndex("_id"));
            name = cursor.getString(cursor.getColumnIndex("name"));
            progress = cursor.getInt(cursor.getColumnIndex("progress"));
            date = cursor.getLong(cursor.getColumnIndex("date"));
            runtime = cursor.getLong(cursor.getColumnIndex("runtime"));
            data.add(toHashMap());
        }
        cursor.close();
        sql.close();
        return this;
    }

    public boolean addDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (name != null)
            values.put("name", name);
        if (progress != null)
            values.put("progress", progress);
        values.put("date", (Long) (System.currentTimeMillis() / 1000L));
        values.put("runtime", (Long) (System.currentTimeMillis() / 1000L));
        sql.insert(tablename, null, values);
        sql.close();
        fromDB(c, "name = '" + name + "' ORDER BY _id DESC LIMIT 1");
        return true;
    }

    public boolean updateDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        if(name != null)
            values.put("name", name);
        if(progress != null)
            values.put("progress", progress);
        if(runtime != null)
            values.put("runtime", runtime);
        sql.update(tablename, values, "_id = " + id, null);
        sql.close();
        return true;
    }

    public void deleteDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getWritableDatabase();
        sql.delete(tablename, param, null);
    }

    @Override
    public String getSpecifiedTableName() {
        return tablename;
    }

    @Override
    public Class<Model> getSpecifiedClass() {
        return null;
    }
}
