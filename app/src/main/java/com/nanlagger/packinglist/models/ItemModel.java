package com.nanlagger.packinglist.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nanlagger.packinglist.dbutils.DBHelper;
import com.nanlagger.packinglist.interfaces.Model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 23.08.14.
 */
public class ItemModel implements Model {

    public Integer id; //id item
    public String name; //name item
    public Integer list_id; //parent's id
    public boolean selected; //true or false picked

    public ArrayList<HashMap<String, Object>> data; //Data model

    protected static final String tablename = "items"; //tablename from DataBase

    public ItemModel() {
        data = new ArrayList<HashMap<String, Object>>();
    }

    @Override
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        args.put("name", name);
        args.put("list_id", list_id);
        args.put("selected", selected);
        return args;
    }

    @Override
    public Model fromDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getReadableDatabase();
        String query = "SELECT * FROM " + tablename;
        if(param != null) {
            query += " WHERE " + param;
        }
        Cursor cursor = sql.rawQuery(query, null);
        if(data != null)
            data.clear();
        while (cursor.moveToNext()) {
            id = cursor.getInt(cursor.getColumnIndex("_id"));
            name = cursor.getString(cursor.getColumnIndex("name"));
            list_id = cursor.getInt(cursor.getColumnIndex("list_id"));
            selected = cursor.getInt(cursor.getColumnIndex("selected")) == 1;
            data.add(toHashMap());
        }
        cursor.close();
        sql.close();
        return this;
    }

    public boolean addDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (name != null)
            values.put("name", name);
        if (list_id != null)
            values.put("list_id", list_id);
        sql.insert(tablename, null, values);
        sql.close();
        fromDB(c, "name = '" + name + "' ORDER BY _id DESC LIMIT 1");
        return true;
    }

    public void deleteDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getWritableDatabase();
        sql.delete(tablename, param, null);
    }

    public boolean updateDB(Context c, String param) {
        DBHelper db = DBHelper.getHelper(c);
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        if(name != null)
            values.put("name", name);
        values.put("selected", selected ? 1 : 0);
        sql.update(tablename, values, "_id = " + id, null);
        sql.close();
        return true;
    }

    @Override
    public String getSpecifiedTableName() {
        return tablename;
    }

    @Override
    public Class<Model> getSpecifiedClass() {
        return null;
    }
}
