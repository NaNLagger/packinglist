package com.nanlagger.packinglist.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.nanlagger.packinglist.MainActivity;
import com.nanlagger.packinglist.R;
import com.nanlagger.packinglist.adapters.ItemsAdapter;
import com.nanlagger.packinglist.adapters.ListsAdapter;
import com.nanlagger.packinglist.models.ItemModel;
import com.nanlagger.packinglist.models.ListModel;

/**
 * Created by root on 24.08.14.
 */
public class ItemsFragment extends Fragment {

    ListView listsView;
    ItemsAdapter adapter;
    ItemModel model;
    ListModel parentModel;

    public static final ItemsFragment newInstance(int id)
    {
        ItemsFragment fragment = new ItemsFragment();
        Bundle bundle = new Bundle(1);
        bundle.putInt("id_model", id);
        fragment.setArguments(bundle);
        return fragment ;
    }

    /*public ItemsFragment(ListModel model) {
        this.parentModel = model;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.parentModel = new ListModel();
        this.parentModel.id = getArguments() != null ? getArguments().getInt("id_model") : 1;
        this.parentModel.fromDB(getActivity(), null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.items_fragment, container, false);
        final EditText addEditText = (EditText) rootView.findViewById(R.id.editText);
        Button addButton = (Button) rootView.findViewById(R.id.button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!addEditText.getText().toString().equalsIgnoreCase("")) {
                    ItemModel model = new ItemModel();
                    model.name = addEditText.getText().toString();
                    model.list_id = parentModel.id;
                    model.addDB(getActivity(), null);
                    updateList();
                    addEditText.setText("");
                }
            }
        });
        setupUI(rootView.findViewById(R.id.parent));
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.lists_bar);
        View actionBarView = actionBar.getCustomView();

        TextView title = (TextView) actionBarView.findViewById(R.id.textView);
        title.setText("" + parentModel.name + " " + parentModel.progress + "%");

        View view = getView();

        listsView = (ListView) view.findViewById(R.id.listView);

        listsView.setFocusable(false); //FUCK YOU ANDROID, I FUCK YOU AND FUCK YOUR FAMILY. THIS PROPERTY NOT WORK IN XML LAYOUT. WHY, BITCH? WHY?


        model = new ItemModel();
        model.fromDB(getActivity(), "list_id = " + parentModel.id);
        adapter = new ItemsAdapter(getActivity(), model, this);
        adapter.notifyDataSetChanged();
        listsView.setAdapter(adapter);
        /*listsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("Navigation", "ListClick");
                ((MainActivity) getActivity()).switchFragment(new ItemsFragment());
            }
        });*/
    }

    public void updateList() {
        model.fromDB(getActivity(), "list_id = " + parentModel.id);
        adapter.notifyDataSetChanged();
        listsView.invalidateViews();
        listsView.refreshDrawableState();
    }

    public void updateActionBar() {
        parentModel.fromDB(getActivity(), "_id = " + parentModel.id);
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();

        TextView title = (TextView) actionBarView.findViewById(R.id.textView);
        title.setText("" + parentModel.name + " " + parentModel.progress + "%");
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }
}
