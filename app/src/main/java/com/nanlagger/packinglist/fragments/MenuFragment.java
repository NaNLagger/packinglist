package com.nanlagger.packinglist.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.nanlagger.packinglist.MainActivity;
import com.nanlagger.packinglist.R;
import com.nanlagger.packinglist.adapters.ListsAdapter;
import com.nanlagger.packinglist.adapters.MenuAdapter;
import com.nanlagger.packinglist.models.ListModel;

import java.util.HashMap;

/**
 * Created by root on 20.08.14.
 */
public class MenuFragment extends Fragment {

    ListView listsView;
    MenuAdapter adapter;
    ListModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.lists_fragment, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        View view = getView();

        listsView = (ListView) view.findViewById(R.id.listView);

        listsView.setFocusable(false); //FUCK YOU ANDROID, I FUCK YOU AND FUCK YOUR FAMILY. THIS PROPERTY NOT WORK IN XML LAYOUT. WHY, BITCH? WHY?

        model = new ListModel();
        model.fromDB(getActivity(), "_id >= 0 ORDER BY runtime DESC LIMIT 5");
        adapter = new MenuAdapter(getActivity(), model);
        adapter.notifyDataSetChanged();
        listsView.setAdapter(adapter);
        listsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("Navigation", "ListClick");
                int idList = (Integer) ((HashMap<String, Object>) adapter.getItem(position)).get("id");
                ((MainActivity) getActivity()).switchFragment(ItemsFragment.newInstance(idList));
            }
        });
    }
}
