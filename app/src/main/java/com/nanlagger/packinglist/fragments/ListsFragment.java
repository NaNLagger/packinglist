package com.nanlagger.packinglist.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.nanlagger.packinglist.MainActivity;
import com.nanlagger.packinglist.R;
import com.nanlagger.packinglist.adapters.ListsAdapter;
import com.nanlagger.packinglist.models.ItemModel;
import com.nanlagger.packinglist.models.ListModel;

import java.util.HashMap;

/**
 * Created by NaNLagger on 23.08.14.
 */

public class ListsFragment extends Fragment {
    ListView listsView;
    ListsAdapter adapter;
    ListModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.lists_fragment, container, false);
        final EditText addEditText = (EditText) rootView.findViewById(R.id.editText);
        Button addButton = (Button) rootView.findViewById(R.id.button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!addEditText.getText().toString().equalsIgnoreCase("")) {
                    ListModel model = new ListModel();
                    model.name = addEditText.getText().toString();
                    model.addDB(getActivity(), null);
                    updateList();
                    addEditText.setText("");
                }
            }
        });
        setupUI(rootView.findViewById(R.id.parent));
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.lists_bar);
        View actionBarView = actionBar.getCustomView();
        /*final ImageButton addButton = (ImageButton) actionBarView.findViewById(R.id.imageButton);
        addButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        addButton.setBackgroundResource(R.color.action_bar_pressed);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        addButton.setBackgroundResource(R.color.action_bar);
                        break;
                    case MotionEvent.ACTION_UP:
                        addButton.setBackgroundResource(R.color.action_bar);
                        break;
                }
                return false;
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        TextView title = (TextView) actionBarView.findViewById(R.id.textView);
        title.setText("All lists");

        View view = getView();

        listsView = (ListView) view.findViewById(R.id.listView);

        listsView.setFocusable(false); //FUCK YOU ANDROID, I FUCK YOU AND FUCK YOUR FAMILY. THIS PROPERTY NOT WORK IN XML LAYOUT. WHY, BITCH? WHY?


        model = new ListModel();
        model.fromDB(getActivity(), "_id >= 0 ORDER BY _id DESC");
        adapter = new ListsAdapter(getActivity(), model, this);
        adapter.notifyDataSetChanged();
        listsView.setAdapter(adapter);
        listsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("Navigation", "ListClick");
                int idList = (Integer) ((HashMap<String, Object>) adapter.getItem(position)).get("id");
                ((MainActivity) getActivity()).switchFragment(ItemsFragment.newInstance(idList));
            }
        });
    }

    public void updateList() {
        model.fromDB(getActivity(), "_id >= 0 ORDER BY _id DESC");
        adapter.notifyDataSetChanged();
        listsView.invalidateViews();
        listsView.refreshDrawableState();
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }
}
