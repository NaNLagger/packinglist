
-- Table: lists
CREATE TABLE lists ( 
    _id      INTEGER PRIMARY KEY AUTOINCREMENT
                     NOT NULL,
    name     TEXT    NOT NULL,
    progress INTEGER DEFAULT ( 0 ) 
);


-- Table: items
CREATE TABLE items ( 
    _id      INTEGER PRIMARY KEY AUTOINCREMENT
                     NOT NULL,
    name     TEXT    NOT NULL,
    list_id  INTEGER DEFAULT ( 0 ),
    selected INTEGER DEFAULT ( 0 ) 
);

